-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 25, 2016 at 08:15 PM
-- Server version: 5.6.17
-- PHP Version: 5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `campussoft`
--

-- --------------------------------------------------------

--
-- Table structure for table `batch`
--

CREATE TABLE IF NOT EXISTS `batch` (
  `batch_id` int(5) NOT NULL AUTO_INCREMENT,
  `course_id` varchar(5) DEFAULT NULL,
  `year_adn` int(5) DEFAULT NULL,
  PRIMARY KEY (`batch_id`),
  KEY `batch` (`course_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `batch`
--

INSERT INTO `batch` (`batch_id`, `course_id`, `year_adn`) VALUES
(1, '1', 2015),
(2, '1', 2016),
(4, '10', 2016),
(5, '10', 2015),
(6, '13', 2013);

-- --------------------------------------------------------

--
-- Table structure for table `courses`
--

CREATE TABLE IF NOT EXISTS `courses` (
  `course_id` varchar(2) NOT NULL,
  `course` varchar(10) NOT NULL,
  `specialisation` varchar(50) NOT NULL,
  `category` varchar(5) NOT NULL,
  PRIMARY KEY (`course_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `courses`
--

INSERT INTO `courses` (`course_id`, `course`, `specialisation`, `category`) VALUES
('1', 'Btech', 'Civil Engineering', 'ug'),
('10', 'Mtech', 'Advanced Communication & Information System', 'pg'),
('11', 'Mtech', 'Advanced Electronics & Communication', 'pg'),
('12', 'Mtech', 'Computer Science & Engineering', 'pg'),
('13', 'MCA', 'Computer Application', 'pg'),
('2', 'Btech', 'Mechanical Engineering', 'ug'),
('3', 'Btech', 'Electrical & Electronics Engineering', 'ug'),
('4', 'Btech', 'Electronics & Communication Engineering', 'ug'),
('5', 'Btech', 'Computer Science & Engineering', 'ug'),
('6', 'Barch', 'Architecture', 'ug'),
('7', 'Mtech', 'Industrial Engineering & Management', 'pg'),
('8', 'Mtech', 'Industrial Drives & Control', 'pg'),
('9', 'Mtech', 'Transportation Engineering', 'pg');

-- --------------------------------------------------------

--
-- Table structure for table `course_sem`
--

CREATE TABLE IF NOT EXISTS `course_sem` (
  `cs_id` int(11) NOT NULL AUTO_INCREMENT,
  `courses` varchar(20) NOT NULL,
  `no_sem` int(2) NOT NULL,
  PRIMARY KEY (`cs_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `course_sem`
--

INSERT INTO `course_sem` (`cs_id`, `courses`, `no_sem`) VALUES
(1, 'Btech', 8),
(2, 'Barch', 10),
(3, 'Mtech', 4),
(4, 'MCA', 6);

-- --------------------------------------------------------

--
-- Table structure for table `login`
--

CREATE TABLE IF NOT EXISTS `login` (
  `username` varchar(20) NOT NULL,
  `password` varchar(20) NOT NULL,
  `role` varchar(20) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `login`
--

INSERT INTO `login` (`username`, `password`, `role`) VALUES
('staff', 'staff', ''),
('admin', 'admin', '');

-- --------------------------------------------------------

--
-- Table structure for table `pg`
--

CREATE TABLE IF NOT EXISTS `pg` (
  `pg_id` int(10) NOT NULL AUTO_INCREMENT,
  `admno` int(5) NOT NULL,
  `roll_no` text,
  `rank` text,
  `quota` text,
  `school_1` text,
  `reg_no_yr_1` text,
  `board_1` text,
  `school_2` text,
  `reg_no_yr_2` text,
  `board_2` text,
  `no_chance` text,
  `name_last` text,
  `total` text,
  `cur_sem` text,
  PRIMARY KEY (`pg_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `pg`
--

INSERT INTO `pg` (`pg_id`, `admno`, `roll_no`, `rank`, `quota`, `school_1`, `reg_no_yr_1`, `board_1`, `school_2`, `reg_no_yr_2`, `board_2`, `no_chance`, `name_last`, `total`, `cur_sem`) VALUES
(3, 2, '2', '2', '2', '', '', '', '', '', '', '', '', '', 'S2'),
(4, 23, '3', '3', '3', '', '', '', '', '', '', '', '', '', 'S2');

-- --------------------------------------------------------

--
-- Table structure for table `sem`
--

CREATE TABLE IF NOT EXISTS `sem` (
  `sem_id` int(2) NOT NULL,
  `semester` varchar(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sem`
--

INSERT INTO `sem` (`sem_id`, `semester`) VALUES
(1, 'S1'),
(2, 'S2'),
(3, 'S3'),
(4, 'S4'),
(5, 'S5'),
(6, 'S6'),
(7, 'S7'),
(8, 'S8'),
(9, 'S9'),
(10, 'S10');

-- --------------------------------------------------------

--
-- Table structure for table `stud`
--

CREATE TABLE IF NOT EXISTS `stud` (
  `adm_no` int(5) NOT NULL,
  `yr_adm` text,
  `name` text,
  `dob` text,
  `sex` text,
  `religion` text,
  `caste` text,
  `mobile` text,
  `email` text,
  `name_guard` text,
  `relation` text,
  `occupation` text,
  `income` text,
  `address` text,
  `phone` text,
  `image` longblob,
  PRIMARY KEY (`adm_no`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stud`
--

INSERT INTO `stud` (`adm_no`, `yr_adm`, `name`, `dob`, `sex`, `religion`, `caste`, `mobile`, `email`, `name_guard`, `relation`, `occupation`, `income`, `address`, `phone`, `image`) VALUES
(1, '23-07-2016', 'vishnu', '2016-07-04', 'M', 'Hindu', 'ezhava', '', '', '', 'father', '', '', 'aaa', '', 0x313735393461612e6a7067),
(2, '23-07-2016', 'vishnu', '2016-07-10', 'M', 'Hindu', '', '', '', '', 'father', 'gulf', '', 'sS', '', 0x39393237756e6e692e6a7067),
(22, '25-07-2016', 'aa', '2016-07-03', 'M', 'Hindu', '', '', '', '', 'aa', '', '', 'aa', '', 0x323234383961612e6a7067),
(23, '25-07-2016', 'aa', '2016-07-03', 'F', 'Hindu', '', '', '', '', 'aa', '', '', 'aa', '', 0x3233383634756e6e692e6a7067),
(121, '23-07-2016', 'vishnu', '2016-07-03', 'M', 'Hindu', '', '', '', '', 'father', 'gulf', '', 'A', '', 0x3133333431494d472e6a7067);

-- --------------------------------------------------------

--
-- Table structure for table `stud_batch_rel`
--

CREATE TABLE IF NOT EXISTS `stud_batch_rel` (
  `adm_no` int(5) NOT NULL,
  `batch_id` varchar(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `stud_batch_rel`
--

INSERT INTO `stud_batch_rel` (`adm_no`, `batch_id`) VALUES
(1, '1'),
(2, '5'),
(121, '1'),
(22, '1'),
(23, '4');

-- --------------------------------------------------------

--
-- Table structure for table `ug`
--

CREATE TABLE IF NOT EXISTS `ug` (
  `ug_id` int(10) NOT NULL AUTO_INCREMENT,
  `admno` int(5) NOT NULL,
  `roll_no` text,
  `rank` text,
  `quota` text,
  `school_1` text,
  `reg_no_yr_1` text,
  `board_1` text,
  `school_2` text,
  `reg_no_yr_2` text,
  `board_2` text,
  `no_chance` text,
  `name_last` text,
  `total` text,
  `physics` text,
  `chemistry` text,
  `maths` text,
  `cur_sem` text,
  PRIMARY KEY (`ug_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `ug`
--

INSERT INTO `ug` (`ug_id`, `admno`, `roll_no`, `rank`, `quota`, `school_1`, `reg_no_yr_1`, `board_1`, `school_2`, `reg_no_yr_2`, `board_2`, `no_chance`, `name_last`, `total`, `physics`, `chemistry`, `maths`, `cur_sem`) VALUES
(6, 1, '1', '1', '1', '', '', '', '', '', '', '', '', '', '', '', '', 'S1'),
(7, 22, '2', '2', '2', '', '', '', '', '', '', '', '', '', '', '', '', 'S4');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `batch`
--
ALTER TABLE `batch`
  ADD CONSTRAINT `batch_ibfk_1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`course_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
